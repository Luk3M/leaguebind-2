from PySide2 import QtWidgets
from PySide2.QtCore import Qt

class QCheckedListBox(QtWidgets.QListWidget):
    def addCheckItems(self, items):
        widget_items_list = []

        for i in items:
            widget_item = QtWidgets.QListWidgetItem(i)
            widget_item.setCheckState(Qt.Unchecked)
            widget_items_list.append(widget_item)

        for widget_item in widget_items_list:
            self.addItem(widget_item)

    def get_checked_items(self):
        return [str(self.item(i).text()) for i in range(self.count()) if self.item(i).checkState() == Qt.Checked]

class QPushButton(QtWidgets.QPushButton):
    def __init__(self, text="", on_click=None):
        super().__init__(text)

        if callable(on_click):
            self.clicked.connect(lambda: self.on_click(on_click()))

    def on_click(self, *args, **kwargs):
        pass

class QHLine(QtWidgets.QFrame):
    def __init__(self):
        super().__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)