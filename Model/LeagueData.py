import os
from shutil import copyfile
from time import strftime as time_strftime

from Controller import utils


class LeagueData:
    league_directory = ""
    hotkey_strings = []
    mastery_string = "[Ctrl][6]"

    @staticmethod
    def set_league_directory(new_directory):
        LeagueData.league_directory = os.path.normpath(new_directory)

    @staticmethod
    def autodetect_league_directory():
        LeagueData.set_league_directory(utils.autodetect_league_path())
    
    @staticmethod
    def get_league_directory():
        LeagueData.autodetect_league_directory()
        return LeagueData.league_directory

    @staticmethod
    def get_settings_path():
        return os.path.normpath(os.path.join(LeagueData.get_league_directory(), "Config\\PersistedSettings.json"))
    
    @staticmethod
    def backup_settings_file():
        if os.path.isfile(LeagueData.get_settings_path()):
            timestr = time_strftime("%Y%m%d-%H%M%S")
            dst = os.path.join(LeagueData.get_league_directory(), "Config\\PersistedSettings-{}.json".format(timestr))
            copyfile(LeagueData.get_settings_path(), dst)
