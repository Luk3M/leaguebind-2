import json
from os import remove as os_remove
from os.path import exists as os_path_exists
from os.path import isfile as os_path_isfile
from os.path import join as os_path_join
from shutil import copyfile

from Model import CustomQtWidgets
from Model.LeagueData import LeagueData


class BindMoveAttackButton(CustomQtWidgets.QPushButton):
    def set_hotkey_string(self, new_value):
        if new_value != None:
            LeagueData.hotkey_strings = new_value

    def on_click(self, *args, **kwargs):
        self.set_hotkey_string(args[0])

        LeagueData.backup_settings_file()
        
        file_path = LeagueData.get_settings_path()

        with open(file_path) as f:
            data = json.load(f)
            
            # finds evntHudMoveableSelect and changes the value to '[Button 1],[Button 3]'
            # finds evtOnUIMouse1 and changes the value value to '[Button 1],[Button 3]'

            evntHudMoveableSelect = LeagueData.hotkey_strings[0]
            evtPlayerAttackMoveClick = LeagueData.hotkey_strings[1]
            evtPlayerMoveClick = LeagueData.hotkey_strings[2]
            evtPlayerSelectClick = LeagueData.hotkey_strings[3]
            evtOnUIMouse1 = LeagueData.hotkey_strings[4]

            has_config_hud = False
            has_config_ui = False
            has_config_move_click = False
            has_config_select_click = False
            has_config_attack_move = False
            
            for data_file in data['files']:
                if data_file['name'] == 'Input.ini':
                    for section in data_file['sections']:
                        if section['name'] == 'GameEvents':
                            for setting in section['settings']: # TODO: Maybe preppend [Button 1] on value when needed automatically?
                                if setting['name'] == 'evntHudMoveableSelect':
                                    setting['value'] = '{}'.format(evntHudMoveableSelect)
                                    has_config_hud = True
                                elif setting['name'] == 'evtOnUIMouse1':
                                    setting['value'] = '{}'.format(evtOnUIMouse1)
                                    has_config_ui = True
                                elif setting['name'] == 'evtPlayerMoveClick':
                                    setting['value'] = '{}'.format(evtPlayerMoveClick)
                                    has_config_move_click = True
                                elif setting['name'] == 'evtPlayerSelectClick':
                                    setting['value'] = '{}'.format(evtPlayerSelectClick)
                                    has_config_select_click = True
                                elif setting['name'] == 'evtPlayerAttackMoveClick':
                                    setting['value'] = '{}'.format(evtPlayerAttackMoveClick)
                                    has_config_attack_move = True
                            
                            if has_config_hud == False:
                                section['settings'].append({'name': 'evntHudMoveableSelect', 'value': '{}'.format(evntHudMoveableSelect)})
                            if has_config_ui == False:
                                section['settings'].append({'name': 'evtOnUIMouse1', 'value': '{}'.format(evtOnUIMouse1)})
                            if has_config_move_click == False:
                                section['settings'].append({'name': 'evtPlayerMoveClick', 'value': '{}'.format(evtPlayerMoveClick)})
                            if has_config_select_click == False:
                                section['settings'].append({'name': 'evtPlayerSelectClick', 'value': '{}'.format(evtPlayerSelectClick)})
                            if has_config_attack_move == False:
                                section['settings'].append({'name': 'evtPlayerAttackMoveClick', 'value': '{}'.format(evtPlayerAttackMoveClick)})
                            
                        has_config_hud = False
                        has_config_ui = False

                        if section['name'] == 'HUDEvents':
                            for setting in section['settings']:
                                if setting['name'] == 'evntHudMoveableSelect':
                                    setting['value'] = '{}'.format(evntHudMoveableSelect)
                                    has_config_hud = True
                                elif setting['name'] == 'evtOnUIMouse1':
                                    setting['value'] = '{}'.format(evntHudMoveableSelect)
                                    has_config_ui = True

            with open(file_path, 'w') as outfile:
                json.dump(data, outfile, indent=4)

class BindMasteryButton(CustomQtWidgets.QPushButton):
    def set_mastery_string(self, new_value):
        full_string = ''

        for i in new_value:
            full_string += '[' + str(i) + "], "

        if full_string != '':
            full_string = full_string[:-2].lower()
        else:
            full_string = '[Ctrl][6]'
        
        LeagueData.mastery_string = full_string

    def on_click(self, *args, **kwargs):
        self.set_mastery_string(args[0])

        LeagueData.backup_settings_file()
        file_path = LeagueData.get_settings_path()

        with open(file_path) as f:
            data = json.load(f)

            for data_file in data['files']:
                if data_file['name'] == 'Input.ini':
                    for section in data_file['sections']:
                        if section['name'] == 'GameEvents':
                            for setting in section['settings']:
                                if setting['name'] == 'evtChampMasteryDisplay':
                                    setting['value'] = LeagueData.mastery_string
                                    
        with open(file_path, 'w') as outfile:
            json.dump(data, outfile, indent=4)

class DeleteInputIniButton(CustomQtWidgets.QPushButton):
    def on_click(self, *args, **kwargs):
        file_path = os_path_join(LeagueData.get_league_directory(), "Config\\input.ini")

        if os_path_exists(file_path):
            os_remove(file_path)
