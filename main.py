#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv as sys_argv
from sys import exit as sys_exit
from PySide2.QtWidgets import QApplication
from View.main_window import MainWindow

def main():
    app = QApplication(sys_argv)
    main_window = MainWindow()
    main_window.show()

    sys_exit(app.exec_())

if __name__ == '__main__':
    main()