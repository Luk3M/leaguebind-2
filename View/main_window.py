from PySide2 import QtWidgets, QtCore, QtGui
from Model.LeagueData import LeagueData
from Model import CustomQtWidgets, Widgets
import json

class VerificationDialog(QtWidgets.QDialog):
    def __init__(self, parent = None):
        super(VerificationDialog, self).__init__(parent)

        # formatting
        self.setWindowTitle("Verification")

        # widgets
        self.verification_label = QtWidgets.QLabel("Make sure you are logged in on League of Legends before continuing.")

        # buttons
        self.button = QtWidgets.QDialogButtonBox( QtWidgets.QDialogButtonBox.Ok, QtCore.Qt.Horizontal, self)

        # layout
        self.mainLayout = QtWidgets.QVBoxLayout(self)
        self.mainLayout.addWidget(self.verification_label)
        self.mainLayout.addWidget(self.button)
        
        self.button.accepted.connect(self.accept)

        # self.buttons.setEnabled(False)
        self.verification_label.setFocus()

class MainWindow(QtWidgets.QWidget):
    def __init__(self, *args):
        super().__init__(*args)
        self.setup_ui()
        VerificationDialog().exec_()

    def setup_ui(self):
        self.setWindowTitle("LeagueBind 2 by Luk3")
        # self.resize(362, 430)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)

        gridLayout = self.setup_layout(QtWidgets.QGridLayout())
        self.setLayout(gridLayout)

    def setup_layout(self, layout):
        layout = QtWidgets.QGridLayout()

        select_label = QtWidgets.QLabel(self.get_league_label_text())
        layout.addWidget(select_label)

        text_input = QtWidgets.QLineEdit(LeagueData.get_league_directory())
        layout.addWidget(text_input)

        change_button = QtWidgets.QPushButton("Change Directory")
        layout.addWidget(change_button)

        h_line_one = CustomQtWidgets.QHLine()
        layout.addWidget(h_line_one)

        button_label = QtWidgets.QLabel("Choose the configuration for attack move:")
        layout.addWidget(button_label)

        moveable_select_button_label = QtWidgets.QLabel("Minimap hover selection (evntHudMoveableSelect):")
        layout.addWidget(moveable_select_button_label)

        moveable_select_button_configuration = QtWidgets.QLineEdit("[Button 1],[Shift][Button 1]")
        layout.addWidget(moveable_select_button_configuration)

        attack_move_button_label = QtWidgets.QLabel("Attack move button (evtPlayerAttackMoveClick):")
        layout.addWidget(attack_move_button_label)

        attack_move_button_configuration = QtWidgets.QLineEdit("[Button 1]")
        layout.addWidget(attack_move_button_configuration)

        move_click_button_label = QtWidgets.QLabel("Regular move (evtPlayerMoveClick):")
        layout.addWidget(move_click_button_label)

        move_click_button_configuration = QtWidgets.QLineEdit("[Button 2],[Shift][Button 2]")
        layout.addWidget(move_click_button_configuration)

        select_click_button_label = QtWidgets.QLabel("Champion/minion selection display (evtPlayerSelectClick):")
        layout.addWidget(select_click_button_label)

        select_click_button_configuration = QtWidgets.QLineEdit("[Button 1],[Shift][Button 1]")
        layout.addWidget(select_click_button_configuration)

        ui_mouse_button_label = QtWidgets.QLabel("User Interface interaction (shop, items...) (evtOnUIMouse1):")
        layout.addWidget(ui_mouse_button_label)

        ui_mouse_button_configuration = QtWidgets.QLineEdit("[Button 1],[Shift][Button 1]")
        layout.addWidget(ui_mouse_button_configuration)

        button_help_label = QtWidgets.QLabel("e.g. [shift], [key], [ctrl], [key], [alt]\nTo add multiple options for activating attack move click, put a comma in between each one.")
        button_help_label.setStyleSheet("color: gray")
        layout.addWidget(button_help_label)

        bind_move_button = Widgets.BindMoveAttackButton("Apply configuration!", on_click=lambda: [moveable_select_button_configuration.text(), attack_move_button_configuration.text(), move_click_button_configuration.text(), select_click_button_configuration.text(), ui_mouse_button_configuration.text()])
        layout.addWidget(bind_move_button)

        h_line_two = CustomQtWidgets.QHLine()
        layout.addWidget(h_line_two)

        choose_label = QtWidgets.QLabel("Choose all keys the mastery should be displayed:")
        layout.addWidget(choose_label)

        keys_list = CustomQtWidgets.QCheckedListBox()
        keys_list.addCheckItems(["Q", "W", "E", "R", "D", "F"])
        layout.addWidget(keys_list)

        bind_mastery_button = Widgets.BindMasteryButton("Bind mastery emote!", on_click=lambda: keys_list.get_checked_items())
        layout.addWidget(bind_mastery_button)

        h_line_three = CustomQtWidgets.QHLine()
        layout.addWidget(h_line_three)

        delete_input_button = Widgets.DeleteInputIniButton("Delete input.ini", on_click=lambda: "")
        layout.addWidget(delete_input_button)

        h_line_four = CustomQtWidgets.QHLine()
        layout.addWidget(h_line_four)
        credits_label = QtWidgets.QLabel("Made by Luk3 and Ryuu. Based on supers0ak's guide.\nContact on Discord with Luk3#8806 (BR) or supers0ak#4474 (US).")
        layout.addWidget(credits_label)

        return layout

    def get_league_label_text(self):
        if LeagueData.get_league_directory() == "":
            return "Select your League of Legends directory"
        else:
            return "League of Legends directory was autodetected but you can change it:"
