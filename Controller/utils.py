from os import path as os_path

'''
Useful links

https://boards.br.leagueoflegends.com/pt/c/mecanicas-de-jogo-e-balanceamento/pscEjh1Q-maestria
https://forum.br.leagueoflegends.com/t5/Mec%C3%A2nicas-de-Jogo/Maestria-e-Ult/td-p/79308
'''

def autodetect_league_path_windows():
    from win32com.client import Dispatch
    from win32com.shell import shell, shellcon

    default_path = "C:\Riot Games\League of Legends"

    if os_path.isdir(default_path):
        return default_path

    shortcut_path = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\League of Legends"
    shortcut_file = os_path.join(shortcut_path, "League of Legends.lnk")

    if os_path.isdir(shortcut_path) and os_path.exists(shortcut_file):
        shell = Dispatch("WScript.Shell")
        shortcut = shell.CreateShortCut(shortcut_file)
        return shortcut.WorkingDirectory
        
    return None

def autodetect_league_path_linux():
    pass

def autodetect_league_path_mac():
    pass

# if returns False it means the current system is unknown
# if returns None, it means the shortcut path was not detected
def autodetect_league_path():
    import platform

    current_platform = platform.system()

    if current_platform == "Windows":
        return autodetect_league_path_windows()
    elif current_platform == "Linux":
        return autodetect_league_path_linux()
    elif current_platform == "Darwin":
        return autodetect_league_path_mac()
    else:
        return False
