# LeagueBind 2
Easily bind your LoL ability keys to the mastery emote or enable attack move on mouse button.

Source code in https://gitlab.com/Luk3M/leaguebind-2.

Continuation of [LeagueBind](https://github.com/Luk3M/LeagueBind).

Packaging is done with [PyInstaller](https://pythonhosted.org/PyInstaller/index.html).

# How to package it from source code

(Not needed if you have an .exe)

Install the requirements in `requirements.txt`

Package it with `pyinstaller main.spec`